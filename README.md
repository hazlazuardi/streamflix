## StreamFlix – Wishnu Hazmi
https://streamflix-haz.herokuapp.com
<hr />

### How to run this project locally?
1. Make sure you have installed Node JS
2. Open Terminal / CMD
3. Go to the project directory
4. On your Terminal/CMD, type >> npm install
5. Then, to run the project, type >> npm run dev
<br />

### Technology
- Framework: ReactJS
- Styling: MaterialUI v5 beta
- React Hooks: 
  - useEffect
  - useState
  - useReducer
  - useContext
- Serve to improve performance
- Material-UI as the UI Library
  

