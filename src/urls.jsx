import { lazy } from 'react';

const Landing = lazy(() => import('./pages/Landing'));
const Movie = lazy(() => import('./pages/MovieP'));

export const urls = [
	{
		id: 0,
		label: 'Home',
		path: '/',
		component: Landing,
	},
];

export const hiddenUrls = [
    {
		id: 0,
		label: 'detail',
		path: '/:movieId_slug',
		component: Movie,
	},

];
