import { useState } from 'react';
import { useCallback } from 'react';
import { useContext } from 'react';
import { useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import Context from '../Context';
import { fetchMovieDetail, fetchRecommendation, fetchSimilar, fetchCredits } from '../TmdbApi';
import { ratingToPrice } from '../util';

function Movie() {
	let { movieId_slug } = useParams();
	const [ movieId, setMovieId ] = useState(movieId_slug.split('-')[0]);

	const { state, dispatch } = useContext(Context);
	const { balance, owned } = state;
	const history = useHistory();

	const buy = (id) => {
		let newBalance = balance - movie.price;
		let newOwned = owned.concat(movie.id);
		dispatch({
			type: 'buy',
			payload: {
				balance: newBalance,
				owned: newOwned
			}
		});
	};
	const goHome = () => {
		history.push('/');
	};

	const handleClick = (movieId_slug) => {
		history.push(`/${movieId_slug}`);
	};

	const [ credits, setCredits ] = useState([]);
	const fetchCreditsC = useCallback((id) => {
		fetchCredits(id)
			.then((res) => {
				res.data.cast.forEach((item) => {
					item['profile_path'] = `https://image.tmdb.org/t/p/w500${item['profile_path']}`;
				});
				setCredits(res.data.cast);
			})
			.catch((e) => {
				console.log(e);
			});
	}, []);
	useEffect(
		() => {
			fetchCreditsC(movieId);
			console.log('mount cast');
		},
		[ fetchCreditsC, movieId ]
	);

	const [ similarResponse, setSimilarResponse] = useState([]);
	const [ similarPage, setSimilarPage ] = useState(1);
	const [ similar, setSimilar ] = useState([]);
	const fetchSimilarC = useCallback(
		(id, page) => {
			fetchSimilar(id, page)
				.then((res) => {
					setSimilar((prev) => {
						// Store new data to an array
						console.log('prev: ', prev);
						const newData = res.data.results;
						console.log('new: ', newData);
						const filteredNewData = newData.filter((a) => !prev.map((b) => b.id).includes(a.id));
						console.log('filtered: ', filteredNewData);
						filteredNewData.forEach((item) => {
							item['poster_path'] = `https://image.tmdb.org/t/p/w500${item['poster_path']}`;
							item['price'] = ratingToPrice(item['vote_average']);
							item['owned'] =
								owned.includes(item.id) ? true :
								false;
						});
						console.log('concat: ', prev.concat(filteredNewData));
						return prev.concat(filteredNewData);
					});
					setSimilarResponse(res.data);
				})
				.catch((e) => {
					console.log(e);
				});
		},
		[ owned ]
	);
	useEffect(
		() => {
			fetchSimilarC(movieId, similarPage);
			console.log('mount similar');
			console.log(similarPage);
		},
		[ fetchSimilarC, movieId, similarPage ]
	);
	const loadMoreSimilar = () => {
		setSimilarPage((prev) => prev + 1);
	};

	const [ recommendationResponse, setRecommendationResponse] = useState([]);
	const [ recommendationPage, setRecommendationPage ] = useState(1);
	const [ recommendation, setRecommendation ] = useState([]);
	const fetchRecommendationC = useCallback(
		(id, page) => {
			fetchRecommendation(id, page)
				.then((res) => {
					setRecommendation((prev) => {
						// Store new data to an array
						console.log('prev: ', prev);
						const newData = res.data.results;
						console.log('new: ', newData);
						const filteredNewData = newData.filter((a) => !prev.map((b) => b.id).includes(a.id));
						console.log('filtered: ', filteredNewData);
						filteredNewData.forEach((item) => {
							item['poster_path'] = `https://image.tmdb.org/t/p/w500${item['poster_path']}`;
							item['price'] = ratingToPrice(item['vote_average']);
							item['owned'] =
								owned.includes(item.id) ? true :
								false;
						});
						console.log('concat: ', prev.concat(filteredNewData));
						return prev.concat(filteredNewData);
					});
					setRecommendationResponse(res.data);
				})
				.catch((e) => {});
		},
		[ owned ]
	);
	useEffect(
		() => {
			fetchRecommendationC(movieId, recommendationPage);
			console.log('mount movie recommendation');
		},
		[ fetchRecommendationC, movieId, recommendationPage ]
	);
	const loadMoreRecommendation = () => {
		setRecommendationPage((prev) => prev + 1);
	};

	const [ movie, setMovie ] = useState([]);
	const fetchMovieDetailC = useCallback(
		(id) => {
			fetchMovieDetail(id)
				.then((res) => {
					res.data.backdrop_path = `https://image.tmdb.org/t/p/w500${res.data.backdrop_path}`;
					res.data.price = ratingToPrice(res.data.vote_average);
					res.data.owned =
						owned.includes(res.data.id) ? true :
						false;
					setMovie(res.data);
				})
				.catch((e) => {});
		},
		[ owned ]
	);
	useEffect(
		() => {
			fetchMovieDetailC(movieId, 1);
			console.log('mount movie detail');
		},
		[ fetchMovieDetailC, movieId ]
	);

	useEffect(
		() => {
			setMovieId(movieId_slug.split('-')[0]);
			console.log('mount movie ID');
		},
		[ movieId_slug ]
	);

	return {
		movie,
		balance,
		buy,
		goHome,
		owned,
		credits,
		recommendation,
		recommendationResponse,
		similar,
		similarResponse,
		similarPage,
		loadMoreSimilar,
		loadMoreRecommendation,
		handleClick,
		recommendationPage,
	};
}

export default Movie;
