import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import React, { lazy, Suspense } from 'react';
import { formatter } from '../util';
import Movie from './Movie';

const HorizontalListMovieP = lazy(() => import('../sections/HorizontalListMovieP'));
const HorizontalListCreditsP = lazy(() => import('../sections/HorizontalListCreditsP'));

function MovieP() {
	const {
		movie,
		buy,
		goHome,
		owned,
		balance,
		credits,
		recommendation,
		recommendationResponse,
		similar,
		similarResponse,
		loadMoreSimilar,
		loadMoreRecommendation,
		handleClick
	} = Movie();

	const handleBuy = () => {
		buy(movie.id);
	};
	return (
		<div>
			<Container>
				<Typography variant="h4">{movie.title}</Typography>
				<hr />
			</Container>
			<Box sx={{ width: '100%' }}>
				<img src={movie.backdrop_path} alt={movie.title} width="100%" />
			</Box>
			<Container>
				<hr />
				<Typography>
					{movie.vote_average} • {movie.runtime} minutes
				</Typography>
				<Typography>{formatter.format(movie.price)}</Typography>
				<hr />
				<Typography>{movie.overview}</Typography>
				<hr />
				<HorizontalListCreditsP credits={credits} title={'Cast'} />
				<hr />
				<Grid container>
					<Grid item xs={6}>
						<Button
							fullWidth
							color="primary"
							variant="contained"
							disabled={owned.includes(movie.id) || balance < movie.price}
							onClick={handleBuy}
						>
							{
								owned.includes(movie.id) ? 'Owned' :
								'Buy'}
						</Button>
					</Grid>
					<Grid item xs={6}>
						<Button fullWidth onClick={goHome}>
							Go to Home
						</Button>
					</Grid>
				</Grid>
				<hr />
				<Suspense fallback={<Typography>Loading..</Typography>}>
					<HorizontalListMovieP
						movies={recommendation}
						title={'Recommendation'}
						loadMore={loadMoreRecommendation}
						handleClick={handleClick}
						response={recommendationResponse}
					/>
					<hr />
				</Suspense>
				<Suspense fallback={<Typography>Loading..</Typography>}>
					<HorizontalListMovieP
						movies={similar}
						title={'Similar Movies'}
						loadMore={loadMoreSimilar}
						handleClick={handleClick}
						response={similarResponse}
					/>
				</Suspense>
				{/* {recommendation.map(movie => (
                <Typography>{movie.title}</Typography>
            ))} */}
			</Container>
		</div>
	);
}

// Judul, poster, rating, casts, durasi film, harga film, indikator apakah pengguna memiliki film tersebut, dll
// • Film serupa
// • Rekomendasi film.

export default MovieP;
