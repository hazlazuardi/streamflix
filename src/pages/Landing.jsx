import { Container } from '@material-ui/core';
import React, { lazy } from 'react';
import { useState } from 'react';
import { useCallback } from 'react';
import { useContext } from 'react';
import { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import Context from '../Context';
import { fetchNowPlaying } from '../TmdbApi';
import { ratingToPrice } from '../util';

const HorizontalListMovieP = lazy(() => import('../sections/HorizontalListMovieP'));

function Landing(props) {
	const { state } = useContext(Context);
	const { owned } = state;
	const [ page, setPage ] = useState(1);
	const history = useHistory();
	
	const [ npResponse, setNpResponse] = useState([]);
	const [ nowPlaying, setNowPlaying ] = useState([]);
	const fetchData = useCallback(
		(counter) => {
			fetchNowPlaying(counter)
				.then((res) => {
					setNowPlaying((prev) => {
						// Store new data to an array
						console.log('prev: ', prev);
						const newData = res.data.results;
						console.log('new: ', newData);
						const filteredNewData = newData.filter((a) => !prev.map((b) => b.id).includes(a.id));
						console.log('filtered: ', filteredNewData);
						filteredNewData.forEach((item) => {
							item['poster_path'] = `https://image.tmdb.org/t/p/w500${item['poster_path']}`;
							item['price'] = ratingToPrice(item['vote_average']);
							item['owned'] =
								owned.includes(item.id) ? true :
								false;
						});
						console.log('concat: ', prev.concat(filteredNewData));
						return prev.concat(filteredNewData);
					});
					setNpResponse(res.data);
				})
				.catch((e) => {});
		},
		[ owned ]
	);

	const loadMore = () => {
		setPage((prev) => (prev += 1));
	};

	const handleClick = (movieId_slug) => {
		history.push(`/${movieId_slug}`);
	};

	useEffect(
		() => {
			fetchData(page);
			console.log('mount landing');
		},
		[ fetchData, page ]
	);

	return (
		<div>
			<Container>
				{/* {props.children} */}
				<HorizontalListMovieP
					title={'Now Playing'}
					movies={nowPlaying}
					setPage={setPage}
					loadMore={loadMore}
					handleClick={handleClick}
					response={npResponse}
				/>
			</Container>
		</div>
	);
}

export default Landing;
