import React from 'react';
import { render, screen } from '../test-utils';
// import { render } from '@testing-library/react';
import HorizontalListCreditsP from './HorizontalListCreditsP';
import { dummyCredits } from '../dummyForTest';

it('Check_Cast_Section_Title', () => {
	const { queryByTitle } = render(<HorizontalListCreditsP credits={dummyCredits} title={'Cast'} />);
	const title = queryByTitle('creditsTitle');
	expect(title).toBeTruthy();
});

it('Check_Cast_Section_Container', () => {
	const { queryByTitle } = render(<HorizontalListCreditsP credits={dummyCredits} title={'Cast'} />);
	const container = queryByTitle('creditsContainer');
	expect(container).toBeTruthy();
});

it('Check_Cast_Section_Card', () => {
	const { queryAllByTitle } = render(<HorizontalListCreditsP credits={dummyCredits} title={'Cast'} />);
	const cards = queryAllByTitle('creditsCard');
	expect(cards).toBeTruthy();
});
