import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import Typography from '@material-ui/core/Typography';
import ImageList from '@material-ui/core/ImageList';
import CardActionArea from '@material-ui/core/CardActionArea';
import ImageListItem from '@material-ui/core/ImageListItem';
import ImageListItemBar from '@material-ui/core/ImageListItemBar';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import { formatter } from '../util';
import CardMedia from '@material-ui/core/CardMedia';

function HorizontalListMovieP(props) {
	const { movies, loadMore, handleClick, title, response } = props;

	const useStyles = makeStyles((theme) => ({
		mainContainer: {
			'::-webkit-scrollbar': { display: 'none' }
		}
	}));

	const classes = useStyles();
	const subtitle = (date, rating, price, owned) => {
		return (
			<div>
				<Typography>{date}</Typography>
				<Typography>
					{rating} • {formatter.format(price)}
				</Typography>
				<Typography>
					{
						owned ? 'Owned' :
						'Buy'}
				</Typography>
			</div>
		);
	};
	return (
		<div>
			<Typography title="moviesTitle" variant="h4">
				{title}
			</Typography>
			<Typography title="moviesRegion">in {process.env.REACT_APP_OMDB_REGION}</Typography>
			<Typography title="moviesPagination">Total: {movies.length}</Typography>
			<Box title="moviesContainer" flex flexWrap className={classes.mainContainer}>
				<ImageList sx={{ width: 'auto', height: 'auto', display: 'flex', flexDirection: 'row' }}>
					{movies &&
						movies.map((data) => (
							<ImageListItem key={data.id}>
								<Card sx={{ minWidth: 200, margin: '8px', height: '100%' }}>
									<CardActionArea
										onClick={() => handleClick(data.id + '-' + data.title.replaceAll(' ', '-'))}
									>
										<CardMedia
											image={data.poster_path}
											sx={{ width: '100%', height: 300 }}
											title={data.title}
										/>
										<ImageListItemBar
											title={data.title}
											subtitle={subtitle(
												data.release_date,
												data.vote_average,
												data.price,
												data.owned
											)}
										/>
									</CardActionArea>
								</Card>
							</ImageListItem>
						))}
					{response.total_results !== movies.length && (
						<ImageListItem>
							<Card variant="outlined" sx={{ width: '100%', height: '100%', minWidth: 200 }}>
								<CardActionArea
									sx={{ width: '100%', height: '100%', minWidth: 200 }}
									onClick={loadMore}
								>
									<Typography textAlign="center">Load More</Typography>
								</CardActionArea>
							</Card>
						</ImageListItem>
					)}{' '}
				</ImageList>
			</Box>
		</div>
	);
}

export default HorizontalListMovieP;
