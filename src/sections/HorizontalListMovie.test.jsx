import React from 'react';
import { render, screen } from '../test-utils';
// import { render } from '@testing-library/react';
import HorizontalListMovieP from './HorizontalListMovieP';
import { dummyMovie, dummyResponse } from '../dummyForTest';

it('Check_Movies_Section_Title', () => {
	const { queryByTitle } = render(<HorizontalListMovieP title={"Now Playing"} movies={dummyMovie} response={dummyResponse}/>);
	const title = queryByTitle('moviesTitle');
	expect(title).toBeTruthy();
});

it('Check_Movies_Section_Container', () => {
	const { queryByTitle } = render(<HorizontalListMovieP title={"Now Playing"} movies={dummyMovie} response={dummyResponse}/>);
	const container = queryByTitle('moviesContainer');
	expect(container).toBeTruthy();
});

it('Check_Movies_Section_Region', () => {
	const { queryByTitle } = render(<HorizontalListMovieP title={"Now Playing"} movies={dummyMovie} response={dummyResponse}/>);
	const region = queryByTitle('moviesRegion');
	expect(region).toBeTruthy();
});

it('Check_Movies_Section_Pagination', () => {
	const { queryByTitle } = render(<HorizontalListMovieP title={"Now Playing"} movies={dummyMovie} response={dummyResponse}/>);
	const pagination = queryByTitle('moviesPagination');
	expect(pagination).toBeTruthy();
});

it('Check_Movie_Section_Card', () => {
	const { queryAllByTitle } = render(<HorizontalListMovieP title={"Now Playing"} movies={dummyMovie} response={dummyResponse}/>);
	const cards = queryAllByTitle('creditsCard');
	expect(cards).toBeTruthy();
});

