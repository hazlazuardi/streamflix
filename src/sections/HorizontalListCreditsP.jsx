import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import ImageListItemBar from '@material-ui/core/ImageListItemBar';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/styles';
import React from 'react';
import CardMedia from '@material-ui/core/CardMedia';
import Card from '@material-ui/core/Card';
import { CardActionArea } from '@material-ui/core';

function HorizontalListCreditsP(props) {
	const { credits, title } = props;

	const useStyles = makeStyles((theme) => ({
		mainContainer: {
			'::-webkit-scrollbar': { display: 'none' }
		}
	}));

	console.log(credits);

	const classes = useStyles();
	const character = (title) => {
		return <Typography variant="h6">{title}</Typography>;
	};
	const subtitle = (name, popularity) => {
		return (
			<div>
				<Typography>{name}</Typography>
				<Typography>Popularity: {popularity}</Typography>
			</div>
		);
	};
	return (
		<div>
			<Typography title="creditsTitle" variant="h4">
				{title}
			</Typography>
			<Box title="creditsContainer" flex flexWrap className={classes.mainContainer}>
				<ImageList sx={{ width: 'auto', height: 'auto', display: 'flex', flexDirection: 'row' }}>
					{credits &&
						credits.map((cast) => (
							<ImageListItem  key={cast.cast_id}>
								<Card title="creditsCard" sx={{ minWidth: 200, margin: '8px', height: '100%' }}>
									<CardActionArea>
										{cast.profile_path && (
											<CardMedia
												image={cast.profile_path}
												sx={{ width: '100%', height: 300 }}
												title={cast.character}
											/>
										)}
										{!cast.profile_path && <Paper sx={{ height: 300 }} />}
										<ImageListItemBar
											title={character(cast.character)}
											subtitle={subtitle(cast.name, cast.popularity)}
										/>
									</CardActionArea>
								</Card>
							</ImageListItem>
						))}
				</ImageList>
			</Box>
		</div>
	);
}

export default HorizontalListCreditsP;
