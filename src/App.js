import React, { Fragment, Suspense, useMemo, useReducer } from 'react';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import { urls, hiddenUrls } from './urls';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import { globalReducer, initialState } from './Reducer';
import Context from './Context';
import { formatter } from './util';

function App() {
	const [ state, dispatch ] = useReducer(globalReducer, initialState);
	const { owned, balance } = state;
	const providerValue = useMemo(() => ({ state, dispatch }), [ state, dispatch ]);

	return (
		<Fragment>
			<Context.Provider value={providerValue}>
				<CssBaseline />
				<Router>
						<Container>
							<Typography variant="h3">StreamFlix</Typography>
							<hr />
							<Typography title='balance' >Your Balance: {formatter.format(balance)}</Typography>
							<Typography>You Own (Movie ID): </Typography>
							{owned.map((movieId, index) => <Typography>- {owned[index]}</Typography>)}
							<hr />
						</Container>
					<Suspense fallback={<Typography>Loading..</Typography>}>
						{urls.map(({ path, component }, key) => (
							<Route exact path={path} component={component} key={key} />
						))}
						{hiddenUrls.map(({ path, component }, key) => (
							<Route exact path={path} component={component} key={key} />
						))}
					</Suspense>
				</Router>
			</Context.Provider>
		</Fragment>
	);
}

export default App;
