import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('Balance Indicator', () => {
	// const [ state, dispatch ] = useReducer(globalReducer, initialState, initialFunction);
	// const providerValue = useMemo(() => ({ state, dispatch }), [ state, dispatch ]);
	render(<App />);
	const UploadBar = screen.getByText('Your Balance: IDR 100,000.00');
	expect(UploadBar).toBeInTheDocument();
});
