const urlBackend = process.env.REACT_APP_BACKEND_URL;
const apiKey = process.env.REACT_APP_OMDB_API_KEY;
const region = process.env.REACT_APP_OMDB_REGION;

const axios = require('axios');

export const fetchNowPlaying = async (page) => {
	const url = urlBackend + '/movie/now_playing';
	const query = url + `?api_key=${apiKey}&language=en-US&page=${page}&region=${region}`

	return await axios.get(query);
};

export const fetchMovieDetail = async (movieId) => {
	const url = urlBackend + `/movie/${movieId}`;
	const query = url + `?api_key=${apiKey}&language=en-US`

	return await axios.get(query);
};

export const fetchRecommendation = async (movieId, page) => {
	const url = urlBackend + `/movie/${movieId}/recommendations`;
	const query = url + `?api_key=${apiKey}&language=en-US&page=${page}`

	return await axios.get(query);
};

export const fetchSimilar = async (movieId, page) => {
	const url = urlBackend + `/movie/${movieId}/similar`;
	const query = url + `?api_key=${apiKey}&language=en-US&page=${page}`

	return await axios.get(query);
};

export const fetchCredits = async (movieId) => {
	const url = urlBackend + `/movie/${movieId}/credits`;
	const query = url + `?api_key=${apiKey}&language=en-US`
	return await axios.get(query);
};



