// useReducer()

const initialState = {
    balance: 100000,
    owned: [],
};

const globalReducer = (state, action) => {
    switch(action.type) {
        case 'buy':
            return {
                ...state,
                balance: action.payload.balance,
                owned: action.payload.owned
            }
        default:
            return state;
    }
};

export { initialState, globalReducer };
